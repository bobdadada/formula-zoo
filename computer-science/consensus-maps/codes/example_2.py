"""
Example shows Consensus Maps without need of Cycle Breaking
"""

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

from consensus_maps import *

fig, ax = plt.subplots(3, 2)

# create and draw graph 1
w1 = np.array([[0, 2, 3, 0], [0, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0]])
draw_digraph_from_matrix(w1, ax=ax[0,0])
ax[0,0].set_title("original directed graph 1")

# create and draw graph 2
w2 = np.array([[0, 1, 0, 0], [0, 0, 0, 1], [0, 0, 0, 0], [0, 0, 1, 0]])
draw_digraph_from_matrix(w2, ax=ax[0,1])
ax[0,1].set_title("original directed graph 2")

# create and draw transitive closure of graph 1
w1 = transitive_closure(w1)
draw_digraph_from_matrix(w1, ax=ax[1,0])
ax[1,0].set_title("transitive closure of graph 1")

# create and draw transitive closure of graph 2
w2 = transitive_closure(w2)
draw_digraph_from_matrix(w2, ax=ax[1,1])
ax[1,1].set_title("transitive closure of graph 2")

# get aggregate graph
aw = aggregate_graph(4, (w1, [0,1,2,3]), (w2, [0,1,2,3]))
draw_digraph_from_matrix(aw, ax=ax[2,0])
ax[2,0].set_title("aggregate graph")

# get transitive reduction of the aggregate graph
tw = transitive_reduction(aw)
draw_digraph_from_matrix(tw, ax=ax[2,1])
ax[2,1].set_title("transitive reduction")

# show
plt.show()