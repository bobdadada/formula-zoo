"""
Simple example shows how to draw a network in Python.
"""

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

# weight matrix
w = np.array([[0, 1, 0, 0], [0, 0, 0, 1], [3, 0, 0, 0], [0, 0, 0, 1]])

# directed graph from the weight matrix
dg = nx.DiGraph(w)

# create a figure to plot the network, network can be rendered by various graphics 
# libraries
plt.figure()
nx.draw_networkx(dg)

# show
plt.show()