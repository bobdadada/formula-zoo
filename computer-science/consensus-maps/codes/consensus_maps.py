
import itertools

import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

def transitive_closure(w):
    """
    Implement the Floyd-Warshal algorithm to generate the weigth matrix
    of the transitive closure. Note we don't need to find a shortest path
    from a specific node, So we only take the first half of the Floyd-Warshall
    algorithm, that is, the Dijkstra algorithm.
    
    Args:
        w: 2D numpy.ndarray which contains the weight of the original directed 
            graph.
        
    Ret:
        2D numpy.ndarray which contains the weight of transitive closure of
        the original graph. Only edges with positive weight are store in the matrix.
    """    
    n = len(w)  # number of nodes
    
    cw = np.copy(w)  # weight of the transitive closure

    # reverse the direction of edge with negative weights
    cw = cw - cw.T
    cw[cw<0] = 0

    cw[np.diag_indices(n, 2)] = 0  # ingore edge (u,u)
    
    for k, i, j in itertools.product(*itertools.repeat(range(n), 3)):
        if i == j or j == k or k == i:
            continue
        if cw[i,k] == 0 or cw[k,j] == 0:
            continue
        else:
            if cw[i,j] == 0 or cw[i,j] > cw[i,k] + cw[k,j]:
                cw[i,j] = cw[i,k] + cw[k,j]
    
    return cw

def aggregate_graph(t, *cwns):
    """
    Integrate all closure weight matrices to obtain the total weight matrix. 
    Nodes are labelled by the global indices.
    
    Args:
        t: total number of nodes
        *cwns: list of (cw, node) tuples where cw is a weight matrix of transitive closure an
            node is 1D array contains the node labels in the global map.
    
    Ret:
        total weight matrix of aggregate transitive closure
    """
    gw = np.zeros((t,t), dtype=cwns[0][0].dtype)  # global matrix
    
    # all sum
    for cw, node in cwns:
        node = tuple(node)
        cs, rs = np.meshgrid(node, node)  # broadcast. get the global index matrix
        gw[rs, cs] += cw
    
    # aggregate matrix, only the dorection with a higher weight will be retained
    gw = gw - gw.T
    gw[gw<0] = 0
    
    return gw


class SimpleCycle:
    """
    SimpleCycle DataType. This type is a immutable, hashable and comparable set. The first
    element in the sequence is the minimum value of the nodes in the simple cycle. Hence,
    the nodes must be comparable.
    """
    def __init__(self, iterable):
        self._data = []
        for value in iterable:
            if value not in self._data:
                self._data.append(value)
            
        self._sort_data()
    
    def __iter__(self):
        return iter(self._data)
    
    def __contains__(self, node):
        return node in self._data
    
    def __len__(self):
        return len(self._data)
    
    def __hash__(self):
        return hash('-'.join(str(d) for d in self._data))
    
    def __eq__(self, other):
        if type(self) != type(other):
            return False
        else:
            return hash(self) == hash(other)
    
    def has_edge(self, edge):
        s, e = edge
        try:
            i = self._data.index(s)
        except ValueError:
            return False
        if i == len(self._data) - 1:
            if e == self._data[0]:
                return True
        elif e == self._data[i+1]:
            return True
        else:
            return False
    
    def iter_edge(self):
        if len(self._data) > 1:
            for i in range(len(self._data)-1):
                yield (self._data[i], self._data[i+1])
            yield (self._data[-1], self._data[0])
    
    def _sort_data(self):
        if self._data:
            i = self._data.index(min(self._data))
            self._data = self._data[i:] + self._data[:i]
    
    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self._data)


def transitive_reduction(aw):
    """
    Get the transitive reduction of a finite directed acyclic graph.
    
    Args:
        aw: weight matrix of a finite directed acyclic graph
    
    Ret:
        weight matrix of the transitive reduction of original matrix
    """    
    n = len(aw)
    
    tw = np.copy(aw)
    
    for k, i, j in itertools.product(*itertools.repeat(range(n), 3)):
        if i != j != k:
            if tw[i,j] > 0 and tw[i,k] > 0 and tw[k,j] > 0:
                tw[i,j] = 0

    return tw

def draw_digraph_from_matrix(m, ax=None):
    """
    Use matplotlib to draw a directed graph from a matrix

    Args:
        m: matrix 
        ax(optional): matplotlib axes abject
    """
    if ax is None:
        ax = plt.gca()
    nx.draw_networkx(nx.DiGraph(m), ax=ax)