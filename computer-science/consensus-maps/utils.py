# -*- coding: utf-8 -*-
'''
包含了各个步骤的函数,
a = [[0, 1, 2, 0],
 [0, 0, 1, 0],
 [0, 0, 0, 1],
 [0, 0, 0, 0]]
'''

import numpy as np
import networkx as nx
import networkx.algorithms as algs

# 所有的输入权重矩阵都转化为np.nparray类型
def npArray(mat):
    return np.array(mat, dtype=np.float)
  

#transitiveClosure Computes the distance matrix from the weight matrix.
#   Use Dijkstra algorithm dij(k)=min{dij(k-1),dik(k-1)+dkj(k-1)};W is the
#   weight matrix. vi is not adjacent to vj, W(i,j)=inf;vi is adjacent to
#   vj, then W(i,j)=f(i,j);W(i,i)=0.
# 由于不需要具体的计算路径，因此我们只取Warshall-Floyd算法的前半部分，即Dijkstra算法。
def transitiveClosure(weightMatrix):
    n = len(weightMatrix)
    closureMatrix = weightMatrix.copy()
    # 注意此处closureMatrix与weightMatrix定义的方式不同。
    closureMatrix[closureMatrix<=0] = np.inf
    for i in range(0, n):
        closureMatrix[i, i] = 0
    # 下面这段代码描绘了如何取寻找最小的连通路径，不过调用了两次循环，代码运行的就比较慢了。
    #可以考虑使用joblib库对多个DAG的weightMatrix进行运算，获得对应的closureMatrix。
    m = 0
    while m < n:
        for i in range(0, n):
            for j in range(0, n):
                if closureMatrix[i,j]>closureMatrix[i,m]+closureMatrix[m,j]:
                    closureMatrix[i,j] = closureMatrix[i,m]+closureMatrix[m,j]
        m = m+1
    # 返回到偏序列的定义权重矩阵的方式
    closureMatrix[closureMatrix==np.inf] = 0
    return closureMatrix

# 将所有的闭包权重矩阵相加，获得总权重，注意：我们默认所有的权重矩阵的节点编号都是一样的。这可以从
# 输入的时候确定下来。
def aggregateGraph(*closureMatrices):
    M = sum(closureMatrices)
    allWeightMatrix = -M.T + M
    allWeightMatrix[allWeightMatrix<0] = 0
    return allWeightMatrix


# 获得强连通有向图
def breakConnectedComponents(DiGraph):
    return nx.strongly_connected_components(DiGraph)
    

# 枚举出一副强连通子图中所有的简单环，我们直接调用networkx库中的所写的Johnson算法。不过我们
# 认为输入的图像已经是强连通的了。返回包含各个子环的列表。
def enumerateCycles(subConnectedMatrix):
    subConnectedGraph = nx.DiGraph(subConnectedMatrix)
    return list(algs.simple_cycles(subConnectedGraph))

###############################################################################
# 将简单环的列表转化成包含边元胞的集set
def nodes2edges(nodesList):
    edgesSet = set()
    u = nodesList[0:]
    v = nodesList[1:]+nodesList[0:1]
    for i,j in zip(u, v):
        edgesSet.add((i,j))
    return edgesSet

# 获得cycles list
def constructCyclesList(cycleElementsLists):
    cycleEdgesSetsList = []
    for cycleElementsList in cycleElementsLists:
        cycleEdgesSetsList.append(nodes2edges(cycleElementsList))
    return cycleEdgesSetsList

# 对cycles list进行进一步的分类，生成字典，字典的key为相应的边
def constructCyclesDict(cycleEdgesSetsList):
    commonCycleEdgesSet = set.union(*cycleEdgesSetsList)
    cyclesDict = {}
    for edge in commonCycleEdgesSet:
        cyclesDict[edge] = []
        for cycleEdgesSet in cycleEdgesSetsList:
            if edge in cycleEdgesSet:
                cyclesDict[edge].append(cycleEdgesSet)
    return cycleEdgesSetsList, commonCycleEdgesSet, cyclesDict

# 比较序列
def listContained(list1, list2):
    for item in list1:
        if item in list2:
            continue
        else:
            return False
    return True

###############################################################################

# 清楚多余的集合
def dataReduction(cycleElementsLists, quene, weightMatrix):
    '''
    cycleEdgesSetsList,
    cyclesDict,
    cycleEdgesList,
    edgesWeightDict
    '''
    cycleEdgesSetsList, commonCycleEdgesSet, cyclesDict = constructCyclesDict(constructCyclesList(cycleElementsLists))
    # 法则1（set cover）
    # 获得每个边的权重，即为cyclesDict每一个cycle set的权重
    edgesWeightDict = {}
    for u,v in commonCycleEdgesSet:
        edgesWeightDict[(u,v)] = weightMatrix[quene[u], quene[v]]   
    # 从CycleEdgesList中确定可以删除的多余数据
    cycleEdgesList = list(commonCycleEdgesSet)
    nofEgdes = len(cycleEdgesList) 
    # 没有可以删除的标志
    flag = True
    while flag:
        # 判断改变数集
        change = False
        for i in range(0,nofEgdes-1):
            edgeI = cycleEdgesList[i]
            for j in range(i+1,nofEgdes):
                edgeJ = cycleEdgesList[j]
                if edgesWeightDict[edgeI] >= edgesWeightDict[edgeJ]:
                    if len(cyclesDict[edgeI]) < len(cyclesDict[edgeJ]):
                        if listContained(cyclesDict[edgeI], cyclesDict[edgeJ]):
                            change = True
                            break
            if change:
                break
        if change:
            del cycleEdgesList[i]
            nofEgdes = len(cycleEdgesList) 
            del cyclesDict[edgeI]
            del edgesWeightDict[edgeI]
        else:
            flag = False
    # 法则2
    flag = True
    for value in cyclesDict.values():
        if len(value) == 1:
            flag = False
            break        
    while flag:
        for cycleEdgesSet in cycleEdgesSetsList:
            # 判断是否删除某个简单环
            remove = True
            for value in cyclesDict.values():
                if cycleEdgesSet not in value:
                    remove = False
                    break
            if remove:
                break
        if remove:
            cycleEdgesSetsList.remove(cycleEdgesSet)
            for value in cyclesDict.values():
                value.remove(cycleEdgesSet)
                if len(value) == 1:
                    flag = False
        else:
            flag = False
    # 法则3
    for cycleEdgesSet in cycleEdgesSetsList:
        tip = None
        times = 0
        for value in cyclesDict.values():
            if cycleEdgesSet in value:
                times += 1
                tip = value
            if times == 2:
                break
        if times == 1:
            if len(tip) > 1:
                tip.clear()
                tip.append(cycleEdgesSet)
    # 返回留下来的环
    return edgesWeightDict, cycleEdgesList, cyclesDict, cycleEdgesSetsList

###############################################################################
def subtractList(list1, list2):
    removed = []
    for item in list2:
        if item in list1:
            list1.remove(item)
            removed.append(item)    
    return removed
###############################################################################

# 使用分支定界算法获得最小的值
def branchAndBound(edgesWeightDict, cycleEdgesList, cyclesDict, cycleEdgesSetsList):
    nofEdges = len(cycleEdgesList)
    # 计算删除边的权重
    global weight, tip
    tip = 0
    weight = np.inf
    edgesRemoved = []
    path = []
    control = [0]
    # 注意copy过后，Set的指向没有变
    cycles = cycleEdgesSetsList.copy()
    edges = cycleEdgesList.copy()
    # 先进行极小化操作，注意cycles是全局变量
    def DFS():
        global weight, tip
        tip += 1
        if tip > 100:
            return None
        # 注意cycles是全局变量，且会在函数内变化
        path.append(subtractList(cycles, cyclesDict[edges[control[-1]]]))
        if not path[-1]:
            if control[-1] != nofEdges-1:
                control[-1] += 1
                path.pop()
                DFS()
                return None          
        else:     
            if not cycles:
                wp = sum([edgesWeightDict[edges[i]] for i in control])
                if weight > wp:
                    weight = wp
                    edgesRemoved.clear()
                    edgesRemoved.extend([edges[i] for i in control])
                if control[-1] != nofEdges-1:
                    cycles.extend(path[-1])
                    control[-1] += 1
                    path.pop()
                    DFS()
                    return None
                else:
                    cycles.extend(path[-1])
            else:
                if control[-1] != nofEdges-1:
                    control.append(control[-1]+1)
                    DFS()
                    return None
                else:
                    cycles.extend(path[-1])
        if len(control) == 1:
            if control[-1] == nofEdges-1:
                return None
        control.pop()
        path.pop()
        cycles.extend(path[-1])
        path.pop()
        control[-1] += 1
        DFS()
    while weight == np.inf or (len(control) != 1 or control[-1] != nofEdges-1):
        print(str(control))
        tip = 0
        # 运行DFS算法
        DFS()
    del weight, tip
    # 返回移除的边
    return edgesRemoved

# 判断是否为DAG
def isDAG(connectedMatrix):
    g = nx.DiGraph(connectedMatrix)
    return algs.dag.is_directed_acyclic_graph(g)

# 启发式算法
def heuristicCycleBreak(connectedMatrix, N):
    # 获得强连通点数目
    a = len(connectedMatrix)
    # 创建一个字典专门用于记录某个边的环的数目
    edgesDict = {}
    # 记录环的数目
    i = 0
    # 默认寻找2N个环
    while i <= 2*N:
        # 为了增加随机性，我们将connectedMatrix的标志随机排列。
        perm = np.random.permutation(a)
        permRev = np.zeros(a)
        for x,y in enumerate(perm):
            permRev[y] = x
        # 获得排列后的图
        M = connectedMatrix[perm,:][:,perm]
        graph = nx.DiGraph(M)
        # 随机选择开始点
        startNode = np.random.randint(a)        
        cycle = nx.find_cycle(graph, startNode)
        # 将找到的环通过的边记录下来
        for x,y in cycle:
            if not edgesDict.get((permRev[x],permRev[y])):
                edgesDict[(permRev[x],permRev[y])] = 1
            else:
                edgesDict[(permRev[x],permRev[y])] += 1
        i += 1
    # 将通过环数目最多的边提取出来。经过检验，单单删除一条边是完全不够的，算的还是太慢了。
    # 我们删除1条边。
    edges = [s[0] for s in sorted(edgesDict.items(),key=lambda x:x[1],reverse=True)[:1]]
    return edges

# 打开闭包
def transitiveReduction(weightMatrixAcyclic):
    reductionGraph = nx.transitive_reduction(nx.DiGraph(weightMatrixAcyclic))
    return reductionGraph