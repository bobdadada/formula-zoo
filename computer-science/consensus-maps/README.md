

### Summary

**Consensus Maps**通过定义一些规则实现了多个有矛盾的树的整合。**Consensus Maps**的概念由以下学者提出，并率先应用于染色体排序上。

```pdf
_assets/paper/consensus_genetic_maps_a_graph_theoretic_approac.pdf
```

### HowTo

####  Approach

This algorithm consists of the following steps:

1. **Transitive Closure**

A transitive closure of a graph $G=\{V,E\}$ is a graph $G'=\{V',E'\}$ such that $V'=V$ and $E'$ consists of all edges $\{(u,v)|<u,\dots,v>\in G\}$.

Assign a weight to each pair of nodes corresponding to our confidence in their order. Let

- $w(u,v)$ the shortest path from $u$ to $v$ in $G$
- $w(u,v)=-w(v,u)$
- $w(u,v)=0$ if neither $<u,\dots,v>\in G$ nor $<v,\dots,u>\in G$.

Intuitively, the weighting scheme comes directly from our knowledge that local mistakes in order are much more likely than global mistakes. If $v$ directly follows $u$, we are less confident of the order than if $v$ follows $u$ with several markers in between.

A simple example shown below

![Transitive Closure](pics/transitive_closure.svg)

Use classic Floyd-Warshal $O(n^3)$ to implement this process in Python

```python
import itertools
import numpy as np

def transitive_closure(w):
    """
    Implement the Floyd-Warshal algorithm to generate the weigth matrix
    of the transitive closure. Note we don't need to find a shortest path
    from a specific node, So we only take the first half of the Floyd-Warshall
    algorithm, that is, the Dijkstra algorithm.
    
    Args:
        w: 2D numpy.ndarray which contains the weight of the original directed 
            graph.
        
    Ret:
        2D numpy.ndarray which contains the weight of transitive closure of
        the original graph. Only edges with positive weight are store in the matrix.
    """    
    n = len(w)  # number of nodes
    
    cw = np.copy(w)  # weight of the transitive closure

    # reverse the direction of edge with negative weights
    cw = cw - cw.T
    cw[cw<0] = 0

    cw[np.diag_indices(n, 2)] = 0  # ingore edge (u,u)
    
    for k, i, j in itertools.product(*itertools.repeat(range(n), 3)):
        if i == j or j == k or k == i:
            continue
        if cw[i,k] == 0 or cw[k,j] == 0:
            continue
        else:
            if cw[i,j] == 0 or cw[i,j] > cw[i,k] + cw[k,j]:
                cw[i,j] = cw[i,k] + cw[k,j]
    
    return cw
```



2. **Aggregate Graph**

Create a global index for identifying markers. The set of all markers in the input maps $U=V_1\cup V_2\cup \cdots V_n$ be the global marker index of size $\|U\|=t$. Construct a global map $G_A$ by taking the summation $M_A=\sum^{n}_{i=1}G'_i$. The nodes of $G_A$ are al markers in the universe.

Intuitively, an edge $(u,v)$ exists in $G_A$ if $M_A[u,v]>0$. $M_A$ can be thought as the results of a vote.

A simple example shown below

![Aggregate Graph](pics/aggregate_graph.svg)

Implementation in Python

```python
import numpy as np

def aggregate_graph(t, *cwns):
    """
    Integrate all closure weight matrices to obtain the total weight matrix. 
    Nodes are labelled by the global indices.
    
    Args:
        t: total number of nodes
        *cwns: list of (cw, node) tuples where cw is a weight matrix of transitive closure an
            node is 1D array contains the node labels in the global map.
    
    Ret:
        total weight matrix of aggregate transitive closure
    """
    gw = np.zeros((t,t), dtype=cwns[0][0].dtype)  # global matrix
    
    # all sum
    for cw, node in cwns:
        node = tuple(node)
        cs, rs = np.meshgrid(node, node)  # broadcast. get the global index matrix
        gw[rs, cs] += cw
    
    # aggregate matrix, only the dorection with a higher weight will be retained
    gw = gw - gw.T
    gw[gw<0] = 0
    
    return gw
```



3. **Cycle Breaking**

When the inconsistency exists in the original graphs, the cycle such as $<u,v,w,u>$ may appear in the aggregate graph and no final consensus order can be calculated before the cycle is broken. We wish to remove those orderings that we have the least confidence in, which corresponds to the smallest weight. To optimally break cycles, we removet hat set of edges $E_c$ such that the sum of all weights on those edges is minimum and all cycles are broken. The cycle breaking problem is known to be NP-hard.

Think this problem as a weighted set cover problem. Let $U_C$ be the set of all simple cycles in $G_A$ and $C(u,v)$  be the set of cycles of the form $<a,\dots,u,v,\dots,a>$, that is all cycles containinng the edge $<u,v>$. In order to break all the cycles, we must find a set of edges $E_c$ to remove from the graph, such that $\bigcup_{(u_i,v_j)\in E_c}C(u_i,v_j)=U_{C}$. In order to break cycles optimally, we want $\sum_{(u_i,v_j)\in E_c}w(u_i,v_j)$ to be minimized.

- datatype of simple cycle

  ```python
  class SimpleCycle:
      """
      SimpleCycle DataType. This type is a immutable, hashable and comparable set. The first
      element in the sequence is the minimum value of the nodes in the simple cycle. Hence,
      the nodes must be comparable.
      """
      def __init__(self, iterable):
          self._data = []
          for value in iterable:
              if value not in self._data:
                  self._data.append(value)
              
          self._sort_data()
      
      def __iter__(self):
          return iter(self._data)
      
      def __contains__(self, node):
          return node in self._data
      
      def __len__(self):
          return len(self._data)
      
      def __hash__(self):
          return hash('-'.join(str(d) for d in self._data))
      
      def __eq__(self, other):
          if type(self) != type(other):
              return False
          else:
              return hash(self) == hash(other)
      
      def has_edge(self, edge):
          s, e = edge
          try:
              i = self._data.index(s)
          except ValueError:
              return False
          if i == len(self._data) - 1:
              if e == self._data[0]:
                  return True
          elif e == self._data[i+1]:
              return True
          else:
              return False
      
      def iter_edge(self):
          if len(self._data) > 1:
              for i in range(len(self._data)-1):
                  yield (self._data[i], self._data[i+1])
              yield (self._data[-1], self._data[0])
      
      def _sort_data(self):
          if self._data:
              i = self._data.index(min(self._data))
              self._data = self._data[i:] + self._data[:i]
      
      def __repr__(self):
          return "{}({})".format(self.__class__.__name__, self._data)
  ```

- enumerate the simple cycles

  Use the method proposed by Johnson <a class="refer">[3]</a> which finds the cycles in $O((C+1)(V+E))$ times, where $C$ is the number of simple cycles in the graph.

  ```python
  def simple_cycles(w):
      """
      Find simple cycles from weight matrix of a strongly connected directed 
      graph  by Johnson's method in O(C(V+E)) times, where C is the number
      of simples cycle in th graph.
      
      Args:
          w: 2D numpy.ndarray - weight matrix of a strongly connected directed 
              graph
      
      Ret:
          set of all simple cycles, the simple cycles are an instance of 
          SimpleCycle
      """
      pass
  ```



4. **Transitive Reduction**

Once the cycles are broken in the input graph, the final step of the algorithm is to run a transitive reduction. This can easily be done in $O(n^3)$ time using the conceptual inverse of the Floyd-Warshal algorithm.

After **Cycle Breaking**, we get a finite directed acyclic graph which has a unique transitive reduction. And the transitive reduction coincides with the minimum equivalent graph in this case, which is independently with the weights of edges but dependently with the reachability relation of the graph. See wikepedia: [Transitive Reduction](https://en.wikipedia.org/wiki/Transitive_reduction) and reference <a class="refer">[4]</a>

A simple example shown below

![](pics/transitive_reduction.svg)

Implementation in Python

```python
import itertools
import numpy as np
    
def transitive_reduction(aw):
    """
    Get the transitive reduction of a finite directed acyclic graph.
    
    Args:
        aw: weight matrix of a finite directed acyclic graph
    
    Ret:
        weight matrix of the transitive reduction of original matrix
    """    
    n = len(aw)
    
    tw = np.copy(aw)
    
    for k, i, j in itertools.product(*itertools.repeat(range(n), 3)):
        if i != j != k:
            if tw[i,j] > 0 and tw[i,k] > 0 and tw[k,j] > 0:
                tw[i,j] = 0
    
    return tw
```

#### Drawing

[networkx](https://networkx.org/) provides many powerful functions. For example, in order to draw a directed graph from the weight matrix, you can use the following code

```python
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

# weight matrix
w = np.array([[0, 1, 0, 0], [0, 0, 0, 1], [3, 0, 0, 0], [0, 0, 0, 1]])

# directed graph from the weight matrix
dg = nx.DiGraph(w)

# create a figure to plot the network, network can be rendered by various graphics 
# libraries
plt.figure()
nx.draw_networkx(dg)

# show
plt.show()
```

Resulting graph:

![example 1](pics/example_1.svg)

For more details, refer to the documents from [networkx](https://networkx.org/).

### Examples

1. <a class='path-append download' href="codes/example_2.py" download="consensus_map_without_cycle_breaking.py">Consensus Map (without Cycle Breaking)</a>

   !> All weight matrices are hidden and can be found in the source file.

![Consensus Map (without Cycle Breaking)](pics/example_2.svg)

-----

### Codes

- <a class='path-append download' href="codes/consensus_maps.py">basic</a>: Use functions provided by *numpy* to implement the entire algorithm. It is sufficient for small networks.
- [based on scipy sparse matrix]
- [based on networkx]

<div id='refer-anchor'></div>

### References

[1]: [Consensus Genetic Maps: A Graph Theoretic Approach](_assets/paper/consensus_genetic_maps_a_graph_theoretic_approac.pdf ':ignore :target=_blank')

[2]: [Consensus Genetic Maps as Median Orders](_assets/paper/consensus_genetic_maps_as_median_orders_from_inco.pdf ':ignore :target=_blank')

[3]: [Finding All the Elementary Circuits of a Directed Graph*](_assets/paper/finding_all_the_elementary_circuits_of_a_directed_graph.pdf ':ignore :target=_blank')

[4]: [The Transitive Reduction of a Directed Graph*](_assets/paper/the_transitive_reduction_of_a_directed_graph.pdf ':ignore :target=_blank')

