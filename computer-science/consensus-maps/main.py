# -*- coding: utf-8 -*-
'''
主程序，所有的权重矩阵必须是偏序生成，且没有负数。
'''

# 导入相关库
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
from utils import (npArray, transitiveClosure, aggregateGraph, 
                   breakConnectedComponents, enumerateCycles,
                   dataReduction, branchAndBound, heuristicCycleBreak,
                   transitiveReduction)



class Consensus(object):
    # 当节点的数目大于MaxNodes时，使用启发式算法分开连通分量
    MaxNodes = 80
    
    allWeightMatrix = None
    closureMatrices = None
    
    weightMatrices = None 
    allWeightMatrixAcyclic = None
    edgesRemoved = None
    _quene = None
    
    # 输入的权重矩阵必须是偏序的，否则算法会出现错误
    def __init__(self, *weightMatrices):
        self.weightMatrices = []
        self.weightMatrices.extend([npArray(weightMatrix) for weightMatrix in weightMatrices])
    
    # 将权重矩阵转化成边列表
    def matrix2edges(self, weightMatrix):
        return np.argwhere(weightMatrix)
  
    def transitiveClosure(self):
        self.closureMatrices = []
        for weightMatrix in self.weightMatrices:
            closureMatrix = transitiveClosure(weightMatrix)
            self.closureMatrices.append(closureMatrix)
    
    # 当输入矩阵太大的时候，恢复整个图时候需要输入allsize的尺寸，否则的话需要默认为
    # self.indexes中最大的序号+1。
    def aggregateGraph(self):
        self.allWeightMatrix = aggregateGraph(*self.closureMatrices)
        # 复制一个allWeightMatrix专门用来执行breakCycle
        self.allWeightMatrixAcyclic = self.allWeightMatrix.copy() 
        # 建立一个TempMatrix，便于后续的操作
        self._weightMatrixTemp = self.allWeightMatrixAcyclic.copy()
    
    # 当矩阵太大的时候，我们在类外面完成操作，并将获得的allWeightMatrix赋给类
    def setAllWeightMatrix(self, allWeightMatrix):
        self.allWeightMatrix = allWeightMatrix
        # 复制一个allWeightMatrix专门用来执行breakCycle
        self.allWeightMatrixAcyclic = self.allWeightMatrix.copy() 
        # 建立一个TempMatrix，便于后续的操作
        self._weightMatrixTemp = self.allWeightMatrixAcyclic.copy()
        
    # 增大递归  
    _heuristic = True
    def breakCycle(self):
        while self._heuristic:
            self._breakCycle()
    def _breakCycle(self):
        if self.edgesRemoved == None:
            self.edgesRemoved = []
            self._quene = []
        if self.allWeightMatrixAcyclic.any() == None:
            raise Exception("No Weight Matrix")
        else:
            for quene in breakConnectedComponents(nx.DiGraph(self._weightMatrixTemp)):
                if len(quene) == 1:
                    continue
                elif len(quene) >= self.MaxNodes:
                    print('进入启发式算法')
                    # 启发式算法移除边
                    quene = list(quene)
                    # 删除已经取出来的连通后，重新进入循环
                    self._weightMatrixTemp[self._quene,:] = 0
                    self._weightMatrixTemp[:, self._quene] = 0
                    # 注意子图的的标记与母图不同，需要通过quene转换
                    subConnectedMatrix = self._weightMatrixTemp[quene,:][:,quene]
                    # 将移除的边提取出来
                    subEdgesRemoved = heuristicCycleBreak(subConnectedMatrix, self.MaxNodes)
                    edgesRemoved = [(quene[int(subEdgeRemoved[0])], quene[int(subEdgeRemoved[1])]) for subEdgeRemoved in subEdgesRemoved]
                    # 移除边
                    self.edgesRemoved.extend(edgesRemoved)
                    for edgeRemoved in edgesRemoved:
                        self._weightMatrixTemp[edgeRemoved[0], edgeRemoved[1]] = 0
                        self.allWeightMatrixAcyclic[edgeRemoved[0], edgeRemoved[1]] = 0
                    # 重新进入breakCycle操作
                    self._heuristic = True
                    return
                else:
                    quene = list(quene)
                    self._quene.extend(quene)
                    M = self._weightMatrixTemp[quene,:]
                    # 注意子图的的标记与母图不同，需要通过quene转换
                    subConnectedMatrix = M[:,quene]
                    # 返回具有多个简单环的元素列的列表
                    subCycleElementsLists = enumerateCycles(subConnectedMatrix)
                    # 返回子图环的分类集合和相应的权重
                    subEdgesWeightDict, subCycleEdgesList, subCyclesDict, subCycleEdgesSetsList = dataReduction(
                            subCycleElementsLists, quene, self._weightMatrixTemp)
                    # 使用DFS和分支定界法返回删除的边
                    subRemovedEdges = branchAndBound(subEdgesWeightDict,
                                                subCycleEdgesList, subCyclesDict, subCycleEdgesSetsList)
                    # 从子图的坐标转换成母图的坐标，并删除特定的边
                    for u,v in subRemovedEdges:
                        self.edgesRemoved.append((quene[u],quene[v]))
                        self.allWeightMatrixAcyclic[quene[u],quene[v]] = 0
            self._heuristic = False
    
    def transitiveReduction(self):
        consensusGraph = transitiveReduction(self.allWeightMatrixAcyclic)
        M = nx.to_numpy_array(consensusGraph)
        self.consensusMatrix = self.allWeightMatrixAcyclic.copy()
        self.consensusMatrix[M==0] = 0
                    
                    
if __name__ == '__main__':
    # 输入a
    a = [[0, 1, 0, 0], [0, 0, 0, 1], [3, 0, 0, 0], [0, 0, 0, 1]]
    # 绘制输入图a
    plt.figure()
    nx.draw_networkx(nx.DiGraph(npArray(a)))
    # 输入b
    b = [[0, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 0], [0, 1.5, 0, 0]]
    # 绘制输入图b
    plt.figure()
    nx.draw_networkx(nx.DiGraph(npArray(b)))
    # 运行算法
    consensus = Consensus(a,b)
    consensus.transitiveClosure()
    consensus.aggregateGraph()
    consensus.breakCycle()
    consensus.transitiveReduction()
    # 绘制合并后的图
    plt.figure()
    nx.draw_networkx(nx.DiGraph(consensus.allWeightMatrix))
    # 绘制打开环的图
    plt.figure()
    nx.draw_networkx(nx.DiGraph(consensus.allWeightMatrixAcyclic))
    # 绘制最终结果
    plt.figure()
    nx.draw_networkx(nx.DiGraph(consensus.consensusMatrix))
    plt.show()
