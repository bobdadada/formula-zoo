"""
Implementation of minimum heap in Python 
"""

class HeapEmptyException(Exception):
    """
    Try to pop an element from an empty heap.
    """

class MinHeap:
    """
    Implementation of minimum heap in Python

    Methods:
        add(node) - Add an element
        pop() - Pop up the root
    
    Class Methods:
        minheapify(iterable) - Create a mim heap with elements in an iterable object.
    """
    def __init__(self):
        self._minheap = []
    
    def add(self, node):
        self._minheap.append(node)

        idx = len(self._minheap) - 1

        while idx != 0:
            idx_p = (idx - 1)//2
            node_p = self._minheap[idx_p]
            if node_p > node:
                self._minheap[idx_p], self._minheap[idx] = node, node_p
                idx = idx_p
            else:
                break

    def pop(self):
        if len(self._minheap) == 0:
            raise HeapEmptyException
        else:
            root = self._minheap[0]  # Get the root

            if len(self._minheap) == 1:
                self._minheap.pop(-1)
            else:
                self._minheap[0] = self._minheap[-1]
                self._minheap.pop(-1)

                idx = 0
                node = self._minheap[0]
            
                while idx < len(self._minheap):

                    idx_c = 2*idx+1
                    if idx_c >= len(self._minheap):
                        # No more children
                        break
                    else:
                        # Compare left and right child nodes
                        if idx_c+1 < len(self._minheap):
                            if self._minheap[idx_c+1] < self._minheap[idx_c]:
                                idx_c = idx_c + 1

                    node_c = self._minheap[idx_c]
                    if node_c < node:
                        # The child node is smaller than the parent node. Thus sink
                        self._minheap[idx], self._minheap[idx_c] = node_c, node
                        idx = idx_c
                    else:
                        break
            
            return root
    
    def __len__(self):
        return len(self._minheap)

    def __str__(self):
        return str(self._minheap)
    
    def __repr__(self):
        return repr(self._minheap)

    @classmethod
    def minheapify(cls, iterable):
        minheap = cls()
        for elem in iterable:
            minheap.add(elem)
        return minheap

def test_minheap():
    import random
    
    lst = [random.randint(0,200) for n in range(20)]
    minheap = MinHeap.minheapify(lst)

    lst.sort()  # sort this list
    for elem in lst:
        assert elem == minheap.pop()

if __name__ == '__main__':
    import pytest

    pytest.main([__file__])
