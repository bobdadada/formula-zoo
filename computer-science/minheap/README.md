The minimum heap is a sorted complete binary tree, in which the data value of any non-terminal node is not greater than the value of its left child node and right child node. In addition to the minimum heap, there are similar data structures such as the maximum heap and the maximum-minimum heap.

The maximum-minimum heap is a binary tree in which the maximum level and the minimum level alternate, that is, the son of the maximum level node belongs to the minimum level, and the child of the minimum level node belongs to the maximum level.

Following figure shows an example of minimum heap.

![example_1](pics/example_1.svg)

The heap structure first occupies the child nodes from left to right by default.

### HowTo

#### Approach

- **Add**

Add operation is a process of floating up a node, as shown in the figure below.

![add node](pics/add_node.svg)

- **Delete**

Delete operation is a sinking process, as shown in the figure below.

![delete root](pics/delete_root.svg)

#### Complexity

Since minimum heap cannot retrieve elements, this data structure is often used in scheduling tasks. Finding an item in a heap is an $O(n)$ operation, but if you already know where it is in the heap, removing it is $O(\log n)$.

### Implementation

We use an array to generate a minimum heap. The starting index is $0$. And if the index of parent node is $i$, then the index of left child node is $2i+1$ and the index of right child node is $2i+1$.

```python
"""
Implementation of minimum heap in Python 
"""

class HeapEmptyException(Exception):
    """
    Try to pop an element from an empty heap.
    """

class MinHeap:
    """
    Implementation of minimum heap in Python

    Methods:
        add(node) - Add an element
        pop() - Pop up the root
    
    Class Methods:
        minheapify(iterable) - Create a mim heap with elements in an iterable object.
    """
    def __init__(self):
        self._minheap = []
    
    def add(self, node):
        self._minheap.append(node)

        idx = len(self._minheap) - 1

        while idx != 0:
            idx_p = (idx - 1)//2
            node_p = self._minheap[idx_p]
            if node_p > node:
                self._minheap[idx_p], self._minheap[idx] = node, node_p
                idx = idx_p
            else:
                break

    def pop(self):
        if len(self._minheap) == 0:
            raise HeapEmptyException
        else:
            root = self._minheap[0]  # Get the root

            if len(self._minheap) == 1:
                self._minheap.pop(-1)
            else:
                self._minheap[0] = self._minheap[-1]
                self._minheap.pop(-1)

                idx = 0
                node = self._minheap[0]
            
                while idx < len(self._minheap):

                    idx_c = 2*idx+1
                    if idx_c >= len(self._minheap):
                        # No more children
                        break
                    else:
                        # Compare left and right child nodes
                        if idx_c+1 < len(self._minheap):
                            if self._minheap[idx_c+1] < self._minheap[idx_c]:
                                idx_c = idx_c + 1

                    node_c = self._minheap[idx_c]
                    if node_c < node:
                        # The child node is smaller than the parent node. Thus sink
                        self._minheap[idx], self._minheap[idx_c] = node_c, node
                        idx = idx_c
                    else:
                        break
            
            return root
    
    def __len__(self):
        return len(self._minheap)

    def __str__(self):
        return str(self._minheap)
    
    def __repr__(self):
        return repr(self._minheap)

    @classmethod
    def minheapify(cls, iterable):
        minheap = cls()
        for elem in iterable:
            minheap.add(elem)
        return minheap
```

### Codes

- <a class="path-append download" href="codes/minheap.py">Python List</a>: A min heap data structure implemented with the python's list object.

