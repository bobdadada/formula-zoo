**Binary tree** is an important type of tree structure. The characteristic of the binary tree is that each node can only have two subtrees at most, and there are left and right subtrees. A binary tree is a set of $n$ finite elements. The set is either empty or consists of an element called the root and two disjoint binary trees called the left subtree and the right subtree. When the set is empty, the binary tree is called an empty binary tree. In a binary tree, an element is also called a node.

**Binary search tree**, it is either an empty tree or a binary tree with the following properties: 

- If its left subtree is not empty, the value of all nodes on the left subtree is less than the value of its root node; 
- If its right subtree is not empty, the values of all nodes on the right subtree are greater than the value of its root node;
- Its left and right subtrees are also binary sorted trees respectively. 

As a classic data structure, binary search tree has the characteristics of fast insertion and deletion operations of linked lists, and the advantage of fast array search; therefore, it is widely used, such as file systems and database systems. Data structure for efficient sorting and retrieval operations

### HowTo

#### Approach

- Put

  ![put_node](pics/put_node.svg)

- Delete

  There are four scenarios

  1. node is not in tree

     ![delete_node_1](pics/delete_node_1.svg)

  2. The deleted node has only one child, so simply delete it and have its parent point to its unique child

     ![delete_node_2](pics/delete_node_2.svg)

  3. The deleted node is the leaf node, directly delete this node

     ![delete_node_3](pics/delete_node_3.svg)

  4. The deleted node has two child nodes, so there are two schemes to choose from:

     - select the minimum node of right subtree to replace the position of the deleted node, and delete the replaced node

       ![delete_node_4](pics/delete_node_4.svg)

     - select the maximum node of left subtree to replace the position of the deleted node, and delete the replaced node

       ![delete_node_5](pics/delete_node_5.svg)

- Search

  ![search_node](pics/search_node.svg)

- Show

  A typical Binary Search Tree is shown below

  ![example_1](pics/example_1.svg)

  - In-order
  
    1. Traverse the left subtree by recursively calling the in-order function.
    2. Display the data part of root element (or current element).
    3. Traverse the right subtree by recursively calling the in-order function.

    ![in_order_example_1](pics/in_order_1.svg)

    In-order displays the binary search tree from small to large nodes.

  - Pre-order
  
    1. Display the data part of root element (or current element).
    2. Traverse the left subtree by recursively calling the pre-order function.
    3. Traverse the right subtree by recursively calling the pre-order function.

    ![pre_order_example_1](pics/pre_order_1.svg)

    The order displayed by pre-order is exactly the order in which the nodes are inserted into the tree.

  - Post-order
  
    1. Traverse the left subtree by recursively calling the post-order function.
    2. Traverse the right subtree by recursively calling the post-order function.
    3. Display the data part of root element (or current element).
  
    ![post_order_example_1](pics/post_order_1.svg)

#### Complexity

The complexity of searching a value in the Binary Search Tree is shown below.

| average time complexity | worst time complexity | best time complexity |
| ----------------------- | --------------------- | -------------------- |
| $O(\log n)$             | $O(n)$                | $O(\log n)$          |



### Codes

- <a href="codes/bst.py" class="download path-append">Python BST</a>: An implementation of binary search tree in Python.

<div id='refer-anchor'></div>

### References

[1]: [CSDN BST 二叉搜索树](https://blog.csdn.net/c_living/article/details/81021510)

[2]: [百科 二叉搜索树](https://baike.baidu.com/item/%E4%BA%8C%E5%8F%89%E6%90%9C%E7%B4%A2%E6%A0%91/7077855?fr=aladdin)