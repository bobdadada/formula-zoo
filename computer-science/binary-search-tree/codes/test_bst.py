from bst import BinarySearchTree

class Test_BinarySearchTree:
    def test_put_delete_tri(self):
        tree = BinarySearchTree()
        tree.put(10, 2)
        tree.put(11, 4)
        tree.put(3, 4)
        assert tree.size == 3
        assert tree.delete(4) is False
        assert tree.delete(3) is True
        assert tree.size == 2
        assert tree.delete(10)
        assert tree.delete(11)
        assert tree.size == 0

    def test_get_successor(self):
        tree = BinarySearchTree()
        tree.put(1)
        tree.put(13)
        tree.put(2)
        tree.put(12)
        tree.put(7)
        assert tree.get_successor(1) == (2, None, 1)
        assert tree.get_successor(4) == (None, None, None)
    
    def test_delete_root(self):
        tree = BinarySearchTree()
        tree.put(11)
        tree.put(10)
        tree.put(9)

        root = tree.root

        assert root == 11

        assert tree.root == 11

        assert tree.delete(1) is False
        assert tree.delete(11)
        assert tree.delete(10)
        assert tree.delete(9)
        
        assert tree.root is None
    
    def test_get_successor_right_subtree(self):
        tree = BinarySearchTree()
        tree.puts([11,1,11,11,9,6,11,13,12,13,13,13])

        assert tree.get_successor(11) == (12, None, 1)
        assert tree.get_successor(12) == (13, None, 4)
        assert tree.get_successor(13) == (None, None, None)
    
    def test_get_successor_no_right_subtree(self):
        tree = BinarySearchTree()
        tree.puts([11,1,11,11,9,6])

        assert tree.get_successor(6) == (9, None, 1)
        assert tree.get_successor(1) == (6, None, 1)
        assert tree.get_successor(11) == (None, None, None)
    
    def test_get_min(self):
        tree = BinarySearchTree()
        tree.puts([11,11,1,11,9,13,6,13,12,13,11,13])

        assert tree.get_min() == (1, None, 1)

        tree = BinarySearchTree()
        assert tree.get_min() == (None, None, None)

    def test_A_type(self):
        tree = BinarySearchTree()
        tree.puts([2,1,4,12,13])

        assert tree.delete(2)
        assert tree.delete(1)
        assert tree.delete(12)
        assert tree.delete(4)
        assert tree.delete(13)
        assert tree.show_in_order() == ''

if __name__ == '__main__':
    import pytest

    pytest.main([__file__])