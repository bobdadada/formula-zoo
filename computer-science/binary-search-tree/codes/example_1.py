from bst import BinarySearchTree

tree = BinarySearchTree()
tree.puts([15,14,12,10,13,19,18,16,25,22])

print('in-order')
print(tree.show_in_order())

print('pre-order')
print(tree.show_pre_order())

print('post-order')
print(tree.show_post_order())
