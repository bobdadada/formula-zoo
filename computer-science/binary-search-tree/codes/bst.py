class BinaryTreeNodeDeleted(Exception):
    """
    The node count is 1 and this node needs to be deleted
    """

class KeyNotSet(Exception):
    """
    Set key first when create a node of tree
    """

class KeyMixIn:

    def set_key(self, key):
        self._key = key

    @property
    def key(self):
        if not getattr(self, '_key'):
            raise KeyNotSet('Please set the key by set_key function.')
        return self._key

    def __gt__(self, other):
        if not hasattr(other, 'key'):
            return self.key > other
        else:
            return self.key > other.key

    def __ge__(self, other):
        if not hasattr(other, 'key'):
            return self.key >= other
        else:
            return self.key >= other.key

    def __eq__(self, other):
        if not hasattr(other, 'key'):
            return self.key == other
        else:
            return self.key == other.key

class BinaryTreeNode(KeyMixIn):
    """
    Node of binary tree, Binary tree is an important type of tree structure. Binary tree is
    characterized by each node can only have at most two subtrees, and there are left and
    right branches. An example is shown below.
                    parent
                      ||
                  key: value | count
                  //         \\
              lchild        rchild
    
    Properties:
        key - key to query this node, which is provided by class KeyMixIn. This key can be used to 
            compare nodes.
        value(default: None) - value of the node. This value is only set by `__init__`.
        count(default: 1) - count of the node, which cannot be set. Use `increase()` and `decrease()` to
            change the count. When the count is less than one after `decrease()`, a BinaryTreeNodeDeleted
            exception is raised.
        parent(default: None) - parent of the node, which is an instance of BinaryTreeNode.
        lchild(default: None) - left child of the node, which is an instance of BinaryTreeNode.
        rchild(default: None) - right child of the node, which is an instance of BinaryTreeNode.

    Methods:
        increase() - increase the property `count`
        decrease() - decrease the property `count`. When `count` is reduced to less than one, an exception
            BinaryTreeNodeDeleted is raised.
        delete() - completely remove this node from the binary tree.
    """

    def __init__(self, key, value=None, count=1, parent=None, lchild=None, rchild=None):
        self.set_key(key)
        self._value = value
        if count >= 1:
            self._count = count 
        else:
            raise ValueError('Count must be larger than or equal to 1.')

        self.parent = parent
        self.lchild = lchild
        self.rchild = rchild

    @property
    def count(self):
        return self._count
    
    @property
    def value(self):
        return self._value

    @property
    def parent(self):
        return self._parent
    
    @parent.setter
    def parent(self, parent):
        if parent is None:
            self._parent = None
        else:
            self._parent = parent

    @property
    def lchild(self):
        return self._lchild
    
    @lchild.setter
    def lchild(self, lchild):
        if lchild is None:
            self._lchild = None
        else:
            self._lchild = lchild
    
    @property
    def rchild(self):
        return self._rchild
    
    @rchild.setter
    def rchild(self, rchild):
        if rchild is None:
            self._rchild = None
        else:
            self._rchild = rchild

    def increase(self):
        self._count += 1
    
    def decrease(self):
        if self._count <= 1:
            raise BinaryTreeNodeDeleted('Node removed.')
        else:
            self._count -= 1

    def __str__(self):
        return "<key {}: value {} | count {}>".format(self.key, self.value, self.count)
    
    def __repr__(self):
        return "BinaryTreeNode({}, {}, {}, parent={}, lchild={}, rchild={})".format(
                self.key, self.value, self.count, self.parent, self.lchild, self.rchild
            )
    
    def delete(self):
        """
        Completely separate the node from the Binary Search Tree. Note that this node is
        only used for binary trees. If the node cannot be put into the binary tree
        correctly, the node cannot be removed correctly.
        """
        if self.parent is not None:
            if self.parent.lchild is self:
                self.parent.lchild = None
            elif self.parent.rchild is self:
                self.parent.rchild = None
            self.parent = None
        
        if self.lchild is not None:
            if self.lchild.parent is self:
                self.lchild.parent = None
            self.lchild = None
        
        if self.rchild is not None:
            if self.rchild.parent is self:
                self.rchild.parent = None
            self.rchild = None

class BinarySearchTree:
    """
    An implementation of the binary search tree, children of each node is instances of
    BinaryTreeNode or None.

    Properties:
        root(default: None) - the root of the BST.
        size(default: 0) - the number of items stored in the tree. Note the number
            of nodes of tree is not equal to the number of items, since the duplicate
            items are stored in the same node.
    
    Methods:
        put(key, value=None) - put a key-value pair to the tree. For the same key,
            there is only a unique value corresponding to the key, and the value is equal
            to the value added at the first time.
        puts(kvs) - put a sequence of key-value pairs into the tree.
        delete(key) - delete a item corresponding to the the key. If the number of the items
            with the same key is equal to zero after delete(key), we need rearrange the BST.
        has_key(key) - determine if the key is in the tree.
        get(key) - get the information of the node corresponding to key
        get_min() - get the information of the minimum node.
        get_max() - get the information of the maximum node.
        get_successor(key) - get the information of the successor of key.
        show_in_order() - show this tree in in-order.
        show_pre_order() - show this tree in pre-order.
        show_post_order() - show this tree in post-order.
    """

    def __init__(self):
        self._root = None
        self._size = 0
    
    @property
    def root(self):
        return self._root
    
    @property
    def size(self):
        return self._size

    def _parse_direction(self, direction):
        if direction == 'left':
            return 'lchild'
        elif direction == 'right':
            return 'rchild'
        else:
            raise ValueError('{} not exist'.format(direction))        

    def _has_child(self, node, direction):
        attr = self._parse_direction(direction)
        if node is not None:
            if getattr(node, attr) is not None:
                return True
        return False

    def _unlink_node(self, parent, child):
        if not (parent and child):
            return
        if parent.lchild == child:
            parent.lchild = None
            child.parent = None
        elif parent.rchild == child:
            parent.rchild = None
            child.parent = None

    def _link_node(self, parent, child, direction):
        attr = self._parse_direction(direction)

        # unlink all connection
        if parent is not None:
            self._unlink_node(parent, getattr(parent, attr))
        if child is not None:
            self._unlink_node(child.parent, child)
        
        if parent is not None:
            setattr(parent, attr, child)
        if child is not None:
            child.parent = parent

    def _get_parent_direction(self, child, omit=None):
        direction = omit
        parent = child.parent
        if parent is not None:
            if parent.rchild == child:
                direction = 'right'
            else:
                direction = 'left'
        return direction

    def _swap_node(self, node1, node2):
        if not (node1 and node2):
            return
        
        p1, d1 = node1.parent, self._get_parent_direction(node1, omit='left')
        l1, r1 = node1.lchild, node1.rchild

        p2, d2 = node2.parent, self._get_parent_direction(node2, omit='left')
        l2, r2 = node2.lchild, node2.rchild

        if p2 is not None and p2 == node1:
            if l1 == node2:
                self._link_node(p1, node2, d1)
                self._link_node(node2, node1, 'left')
                self._link_node(node2, r1, 'right')

                self._link_node(node2, node1, d2)
                self._link_node(node1, l2, 'left')
                self._link_node(node1, r2, 'right')
            else:
                self._link_node(p1, node2, d1)
                self._link_node(node2, l1, 'left')
                self._link_node(node2, node1, 'right')

                self._link_node(node2, node1, d2)
                self._link_node(node1, l2, 'left')
                self._link_node(node1, r2, 'right')    

        elif p1 is not None and p1 == node2:
            if l2 == node1:
                self._link_node(node1, node2, d1)
                self._link_node(node2, l1, 'left')
                self._link_node(node2, r1, 'right')

                self._link_node(p2, node1, d2)
                self._link_node(node1, node2, 'left')
                self._link_node(node1, r2, 'right')
            else:
                self._link_node(node1, node2, d1)
                self._link_node(node2, l1, 'left')
                self._link_node(node2, r1, 'right')

                self._link_node(p2, node1, d2)
                self._link_node(node1, l2, 'left')
                self._link_node(node1, node2, 'right')

        else:
            self._link_node(p1, node2, d1)
            self._link_node(node2, l1, 'left')
            self._link_node(node2, r1, 'right')

            self._link_node(p2, node1, d2)
            self._link_node(node1, l2, 'left')
            self._link_node(node1, r2, 'right')

        if self.root == node1:
            self._root = node2
        elif self.root == node2:
            self._root = node1

    def put(self, key, value=None):
        """
        Put key-value pair into this BST, the value defaults to None.

        Args:
            key: the index of the node, for the same kind of tree, the type of
                index is the same.
            value(default: None): the value of the node. For some trees, the value
                of the node is not important.
        """
        node = BinaryTreeNode(key, value)
        if self.root is None:
            self._root = node
        else:
            self._put(node, self.root)
        self._size += 1

    def puts(self, kvs):
        """
        Put multiple key-value pairs into the BST.

        Args:
            kvs: iterable object. If the element is a sequence object of length 1, add this
                object as a key to the tree. If it is a sequence object with a length of 2,
                it is unpacked in (key, value). If it is not a sequence object, use it as a
                key to add it to the tree.
        """
        for kv in kvs:
            if not hasattr(kv, '__len__'):
                self.put(kv)
            else:
                if len(kv) == 1:
                    self.put(kv[0])
                else:
                    self.put(kv[0], kv[1])

    def _put(self, added_node, node):
        if added_node == node:
            node.increase()
            return

        if added_node < node:
            direction = 'left'
        else:
            direction = 'right' 

        if self._has_child(node, direction):
            self._put(added_node, getattr(node, self._parse_direction(direction)))
        else:
            self._link_node(node, added_node, direction)

    def _get_height(self, node):
        if node is None:
            return 0
        else:
            return 1 + max(self._get_height(node.lchild), self._get_height(node.rchild))

    def _delete_leaf(self, node):
        if node == self.root:
            self._root = None
        node.delete()
    
    def _delete_node_with_single_direction(self, node):
        if node.lchild is not None:
            if node == self.root:
                self._root = node.lchild
            else:
                self._link_node(node.parent, node.lchild, 'left')
        else:
            if node == self.root:
                self._root = node.rchild
            else:
                self._link_node(node.parent, node.rchild, 'right')
        node.delete()
    
    def _delete_node_with_two_direction(self, node):
        if self._get_height(node.lchild) > self._get_height(node.rchild):
            switched_node = self._get_max(node.lchild)
        else:
            switched_node = self._get_min(node.rchild)
        
        self._swap_node(node, switched_node)
        if not (node.lchild is None and node.rchild is None):
            self._delete_node_with_single_direction(node)
        else:
            self._delete_leaf(node)

    def delete(self, key):
        """
        Try to delete or decrease the number of items corresponding to the key.
        If the key is not in the tree, return directly. Otherwise, try to reduce
        the item corresponding to the node or move out of the node directly.
        
        Args:
            key: node index.

        Ret:
            If the node is in the tree, return True, else, return False.
        """
        deleted_node = self._get(key, self.root)

        if deleted_node is None:
            return False
        else:
            try:
                deleted_node.decrease()
            except BinaryTreeNodeDeleted:
                if not (deleted_node.lchild is None or deleted_node.rchild is None):
                    self._delete_node_with_two_direction(deleted_node)
                elif not (deleted_node.lchild is None and deleted_node.rchild is None):
                    self._delete_node_with_single_direction(deleted_node)
                else:
                    self._delete_leaf(deleted_node)

            self._size -= 1
            return True

    def has_key(self, key):
        if self.root is None:
            return False
        else:
            node = self._get(key, self.root)
            if node is None:
                return False
            else:
                return True
    
    def __contain__(self, key):
        return self.has_key(key)

    def get(self, key):
        """
        Get the information of the node in the BST corresponding to the key.

        Args:
            key: node index.
        
        Ret:
            If the key is in the tree, return (value, count) tuple corresponding to the node,
            else, return (None, None)
        """
        if self.root is None:
            return (None, None)
        else:
            node = self._get(key, self.root)
            if node is not None:
                return (node.value, node.count)
            else:
                return (None, None)

    def _get(self, key, node):
        if node is None:
            return None

        if key == node:
            return node

        if key < node:
            direction = 'left'
        else:
            direction = 'right'
        return self._get(key, getattr(node, self._parse_direction(direction)))

    def get_min(self):
        """
        Get the minimum node of the BST.
        
        Ret:
            If the tree is empty, return (None, None, None), else return
            (key, value, count) of the minimum node.
        """
        node = self._get_min(self.root)
        if node is None:
            return (None, None, None)
        else:
            return (node.key, node.value, node.count)

    def _get_min(self, node):
        if node is None:
            return None
        else:
            while node.lchild is not None:
                node = node.lchild
            return node

    def get_max(self):
        """
        Get the maximum node of the BST.
        
        Ret:
            If the tree is empty, return (None, None, None), else return
            (key, value, count) of the maximum node.
        """
        node = self._get_max(self.root)
        if node is None:
            return (None, None, None)
        else:
            return (node.key, node.value, node.count)

    def _get_max(self, node):
        if node is None:
            return None
        else:
            while node.rchild is not None:
                node = node.rchild
            return node

    def get_successor(self, key):
        """
        Get the successor of a node with specific key.

        Args:
            key: node index.
        
        Ret:
            If the successor exists, return (key, value, count) of the successor node,
            else return (None, None, None).
        """
        node = self._get(key, self.root)
        if node is None:
            return (None, None, None)
        else:
            successor = self._get_successor(node)
            if successor is None:
                return (None, None, None)
            else:
                return (successor.key, successor.value, successor.count)

    def _get_successor(self, node):
        if node is None:
            return None
        elif node.rchild is not None:
            return self._get_min(node.rchild)
        else:
            parent = node.parent
            while self._get_parent_direction(node) == 'right':
                node = parent
                parent = parent.parent
            return parent

    def show_in_order(self):
        """
        Show the tree in in-order. The return format is "{left subtree}{node}\n{right subtree}".
        """
        return self._show_in_order(self.root)

    def _show_in_order(self, node):
        if node is None:
            return ''
        else:
            return self._show_in_order(node.lchild) + str(node) + '\n' + self._show_in_order(node.rchild)

    def show_pre_order(self):
        """
        Show the tree in pre-order. The return format is "{node}\n{left subtree}{right subtree}".
        """
        return self._show_pre_order(self.root)

    def _show_pre_order(self, node):
        if node is None:
            return ''
        else:
            return str(node) + '\n' + self._show_pre_order(node.lchild) + self._show_pre_order(node.rchild)

    def show_post_order(self):
        """
        Show the tree in post-order. The return format is "{left subtree}{right subtree}{node}\n".
        """
        return self._show_post_order(self.root)

    def _show_post_order(self, node):
        if node is None:
            return ''
        else:
            return self._show_post_order(node.lchild) + self._show_post_order(node.rchild) + str(node) + '\n'
