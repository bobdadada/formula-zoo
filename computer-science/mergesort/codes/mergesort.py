"""
Use mergesort to sort a list
"""
import operator

def mergesort(lst, inplace=False, reverse=False):
    """
    Implementation of mergesort algorithm for a python's list

    Args:
        lst: a python's list object
        inplace(optional, default: False): inplace flag can be set to
            request the input list to be sorted.
        reverse(optional, default: False): reverse flag can be set to 
            request the result in descending order.
    
    Ret:
        sorted list
    """
    if len(lst) == 1:
        if inplace:
            return lst
        else:
            # create a new list
            return list(lst)

    # separate
    mid_index = len(lst) // 2
    # for lists, use : will generate a new list.
    left_lst = lst[:mid_index]
    right_lst = lst[mid_index:]

    # mergesort for sub-list
    mergesort(left_lst, inplace=True, reverse=reverse)
    mergesort(right_lst, inplace=True, reverse=reverse)

    if reverse:
        op = operator.gt
    else:
        op = operator.lt
    
    # inplace or not
    if inplace:
        lst.clear()
    else:
        lst = []

    # merge two sorted list
    while len(left_lst) > 0 and len(right_lst) > 0:
        if op(left_lst[0], right_lst[0]):
            lst.append(left_lst.pop(0))
        else:
            lst.append(right_lst.pop(0))
    lst.extend(left_lst)
    lst.extend(right_lst)

    return lst

def test_mergesort():
    a0 = [5,1,3,4]
    assert mergesort(a0, inplace=True) is a0
    assert a0==[1,3,4,5]

    a1 = [5,1,3,4]
    out = mergesort(a1, inplace=False, reverse=True)
    assert out==[5,4,3,1]

    a2 = [5,1,3,4]
    out = mergesort(a2)
    assert a2==[5,1,3,4]
    assert out==[1,3,4,5]


if __name__ == '__main__':
    test_mergesort()