"""
Use mergesort to sort 1D numpy.ndarray objects
"""
import operator
import numpy as np

def mergesort_numpy(a, out=None, reverse=False):
    """
    Implementation of mergesort algorithm for a 1D numpy.ndarray object

    Args:
        a: 1D numpy.ndarray
        out(optional): ndarray have the same shape as the expected output
        reverse(optional, default: False): reverse flag can be set to 
            request the result in descending order.
    
    Ret:
        sorted array
    """
    a = np.atleast_1d(a)
    if a.ndim > 1:
        raise ValueError("Input array must be 1D.") 
    if out is None:
        out = np.empty_like(a)
    out = np.atleast_1d(out)
    if out.shape != a.shape:
        raise ValueError("The input and output dimensions are not the same.")

    if len(a) == 1:
        out[0] = a[0]
        return out
    
    # divide
    mid_index = len(a) // 2

    # mergesort for sub-array
    mergesort_numpy(a[:mid_index], out=out[:mid_index], reverse=reverse)
    mergesort_numpy(a[mid_index:], out=out[mid_index:], reverse=reverse)

    if reverse:
        op = operator.gt
    else:
        op = operator.lt

    # additional ~n/2 spaces need to be allocated
    left_arr = out[:mid_index].copy()
    right_arr = out[mid_index:]

    # merge
    i, j, n = 0, 0, 0
    while True:
        if op(left_arr[i], right_arr[j]):
            out[n] = left_arr[i]
            i += 1
        else:
            out[n] = right_arr[j]
            j += 1
        n += 1
        if i == mid_index:
            out[n:] = right_arr[j:]
            break
        elif j == len(out)-mid_index:
            out[n:] = left_arr[i:]
            break

    return out

def test_mergesort_numpy():
    a0 = np.array([5,1,3,4])
    assert mergesort_numpy(a0, out=a0) is a0
    assert all(a0==np.array([1,3,4,5]))

    a1 = np.array([5,1,3,4])
    mergesort_numpy(a1, out=a1, reverse=True)
    assert all(a1==np.array([5,4,3,1]))

    a2 = np.array([5,1,3,4])
    out = mergesort_numpy(a2)
    assert all(a2==np.array([5,1,3,4]))
    assert all(out==np.array([1,3,4,5]))


if __name__ == '__main__':
    test_mergesort_numpy()