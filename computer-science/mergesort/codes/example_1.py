"""
Example demonstrates the mergesort algorithm.
"""
import random
import timeit
import math
import statistics

from mergesort import mergesort

random.seed(20201122)

nsamples = 5
ns = [5**(i+2) for i in range(nsamples)]  # contains the length of different list

def test(n, lst):
    # mergesort inplace
    mergesort(lst)

# get the execution time of mergesort
times = []
for n in ns:
    # create a unsorted list
    lst = [random.randint(1, n) for _ in range(n)]
    # timeit
    times.append(timeit.timeit(lambda: test(n, lst), number=10))

nlogns = [n*math.log(n) for n in ns]

# judge T ~ O(nlog(n))
A = [(times[i+1]-times[i])/(nlogns[i+1]-nlogns[i]) for i in range(nsamples-1)]

mu = statistics.mean(A)
sigma = statistics.stdev(A, mu)

if mu/sigma > 20:  # criteria for judgement
    print('The average time complexity of mergesort is O(nlog(n))')
