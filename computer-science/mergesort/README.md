Merge Sort is an effective sorting algorithm based on merge operations, with an efficiency of $O(n\log n)$. It was first proposed by John von Neumann in 1945. This algorithm is a very typical application using Divide and Conquer, and each layer of divide and conquer recursion can be performed simultaneously.

### HowTo

#### Approach

1. **Top-Down Divide**

![divide](pics/divide.svg)

2. **Down-Top Merge**

![merge](pics/merge.svg)

See reference <a class="refer">[1,2]</a> for more details.

#### Complexity

| average time complexity | worst time complexity | best time complexity | space complexity |
| ----------------------- | --------------------- | -------------------- | ---------------- |
| $O(n\log n)$            | $O(n\log n)$          | $O(n\log n)$         | $O(n)$           |

### Implementation

A simple implementation in Python shown below.

```python
import operator

def mergesort(lst, inplace=False, reverse=False):
    """
    Implementation of mergesort algorithm for a python's list

    Args:
        lst: a python's list object
        inplace(optional, default: False): inplace flag can be set to
            request the input list to be sorted.
        reverse(optional, default: False): reverse flag can be set to 
            request the result in descending order.
    
    Ret:
        sorted list
    """
    if len(lst) == 1:
        if inplace:
            return lst
        else:
            # create a new list
            return list(lst)

    # separate
    mid_index = len(lst) // 2
    # for lists, use : will generate a new list.
    left_lst = lst[:mid_index]
    right_lst = lst[mid_index:]

    # mergesort for sub-list
    mergesort(left_lst, inplace=True, reverse=reverse)
    mergesort(right_lst, inplace=True, reverse=reverse)

    if reverse:
        op = operator.gt
    else:
        op = operator.lt
    
    # inplace or not
    if inplace:
        lst.clear()
    else:
        lst = []

    # merge two sorted list
    while len(left_lst) > 0 and len(right_lst) > 0:
        if op(left_lst[0], right_lst[0]):
            lst.append(left_lst.pop(0))
        else:
            lst.append(right_lst.pop(0))
    lst.extend(left_lst)
    lst.extend(right_lst)

    return lst
```

### Codes

- <a class='path-append download' href="codes/mergesort.py" >Python List</a>: Apply mergesort algorithm for Python's lists.
- <a class='path-append download' href="codes/mergesort_numpy.py">Numpy Array</a>: Apply mergesort algorithm for 1D `numpy.ndarray` objects.
- <a class='path-append download' href="codes/example_1.py" download="compute_time_complexity">Average Time Complexity</a>: Evaluate the average time complexity of the mergesort algorithm.

<div id="refer-anchor"></div>

### References

[1]: [Mergesort Princeton.Edu](https://algs4.cs.princeton.edu/22mergesort/)

[2]: [归并排序(merge sort)算法详解与实现_陈龙 | JUST DO IT.-CSDN博客_merge sort算法](https://blog.csdn.net/ASCIIdragon/article/details/84029878)

