抽象代数（Abstract algebra）又称近世代数（Modern algebra），它产生于十九世纪。伽罗瓦〔1811-1832〕在1832年运用「[群](https://baike.baidu.com/item/群/16174)」的概念彻底解决了用根式求解[代数方程](https://baike.baidu.com/item/代数方程/7806522)的可能性问题。他是第一个提出「群」的概念的数学家，一般称他为近世代数创始人。他使[代数学](https://baike.baidu.com/item/代数学/9896691)由作为解方程的科学转变为研究代数运算结构的科学，即把代数学由[初等代数](https://baike.baidu.com/item/初等代数/344522)时期推向抽象代数。

抽象代数包含[群论](https://baike.baidu.com/item/群论/10980672)、[环论](https://baike.baidu.com/item/环论/10284241)、[伽罗瓦理论](https://baike.baidu.com/item/伽罗瓦理论/2549744)、[格论](https://baike.baidu.com/item/格论/3831710)、[线性代数](https://baike.baidu.com/item/线性代数/800)等许多分支，并与数学其它分支相结合产生了[代数几何](https://baike.baidu.com/item/代数几何/345582)、[代数数论](https://baike.baidu.com/item/代数数论/5919796)、[代数拓扑](https://baike.baidu.com/item/代数拓扑/9813369)、[拓扑群](https://baike.baidu.com/item/拓扑群/6896926)等新的数学学科。抽象代数也是现代计算机理论基础之一。

## 代数结构及特点

![代数结构](images/image-1.png)

![环与域的特性](images/image-2.png)

