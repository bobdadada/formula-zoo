### [PROBLEM](../proofs#proof-1  ':class=path-append')

Suppose $v_{i}$ for $i=1,2,\dots,n$ is a set of orthogonal basis of $n$-dimensional vector space. Let 
$$
\|w_i-v_i\|<\frac{1}{\sqrt{n}}
$$
for $i=1,2,\dots,n$. Then $\{w_i\}$ is a set of basis for $n$-dimensional vector space.

**PROOF**:

Check that $\sum_{i=1}^{n}\alpha_{i}w_i=0$ iff $\alpha_{i}=0$ for $i=1,2,\dots,n$. For any real $\alpha_{i}$'s
$$
\begin{aligned}
\|\sum_{i=1}^{n}\alpha_{i}w_i\|=\|\sum_{i=1}^{n}\alpha_{i}(v_i-w_i)-\sum_{i=1}^{n}\alpha_{i}v_i\| \\
\ge|\|\sum_{i=1}^{n}\alpha_{i}(v_i-w_i)\|-\|\sum_{i=1}^{n}\alpha_{i}v_i\|| \\
=|\|\sum_{i=1}^{n}\alpha_{i}(v_i-w_i)\|-\sqrt{\sum_{i=1}^{n}\alpha^{2}_{i}}|
\end{aligned}
$$
Notice
$$
\|\sum_{i=1}^{n}\alpha_{i}(v_i-w_i)\|\le\sum_{i=1}^{n}|\alpha_{i}|\|(v_i-w_i)\|
$$
and
$$
\sqrt{\sum_{i=1}^{n}\alpha^{2}_{i}}\ge \frac{1}{\sqrt{n}}\sum_{i=1}^{n}|\alpha_{i}|
$$
given by Cauchy-Schwartz Theorem. If $\|w_i-v_i\|<1/\sqrt{n}$, then
$$
\sum_{i=1}^{n}|\alpha_{i}|\|(v_i-w_i)\|\le\frac{1}{\sqrt{n}}\sum_{i=1}^{n}|\alpha_{i}|
$$
The equal sign only holds when $\alpha_1=\alpha_2=\dots=\alpha_n=0$. Thus $\|\sum_{i=1}^{n}\alpha_{i}w_i\|\ge0$, and the equal sign only holds when $\alpha_1=\alpha_2=\dots=\alpha_n=0$. In summary, we have proved that $\sum_{i=1}^{n}\alpha_{i}w_i=0$ iff $\alpha_{i}=0$ for $i=1,2,\dots,n$, which implies that $\{w_i\}$ is a set of basis.

<div align="center" style="font-size: 15px;">
    <a href="#/../proofs?id=proof-1" class='path-append'>Back to Contents</a> 
</div>


