- [证明 1](proofs/proof-1.md ':id=proof-1 :class=path-append')

Suppose $v_{i}$ for $i=1,2,\dots,n$ is a set of orthogonal basis of $n$-dimensional vector space. Let 
$$
\|w_i-v_i\|<\frac{1}{\sqrt{n}}
$$
for $i=1,2,\dots,n$. Then $\{w_i\}$ is a set of basis for $n$-dimensional vector space.
