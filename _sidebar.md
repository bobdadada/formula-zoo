<!-- _sidebar.md --> 

- [Description](description.md)

* [Algebra](algebra/)
  * [Abstract Algebra](algebra/abstract-algebra/)
  * [Linear Space](algebra/linear-space/)
    * [Proofs](algebra/linear-space/proofs.md)
* [Cavity Quantum Electrodynamics](cavity-quantum-electrodynamics/)
  * [Fabry-Perot cavity](cavity-quantum-electrodynamics/fabry–perot-cavity/)
* [Computer Science](computer-science/)
  * [Binary Search Tree](computer-science/binary-search-tree/)
  * [Consensus Maps](computer-science/consensus-maps/)
  * [Merge Sort](computer-science/mergesort/)
  * [Min Heap](computer-science/minheap/)
* [Mechanics](mechanics/)
  * [Mechanics of Materials](mechanics/mechanics-of-materials/)
    * [Spring](mechanics/mechanics-of-materials/spring.md)
* [Wave Optics](wave-optics/)
  * [Hermite-Gaussian Beam](wave-optics/hermite-gaussian-beam/)
* [Stochastic Process](stochastic-process/)
  * [Markov Process](stochastic-process/markov-process/)
    * [Proofs](stochastic-process/markov-process/proofs.md)
---

- **Links**

- [<img src="_assets/img/gitlab.svg" width=15 height=15></img>Gitlab](https://gitlab.com/bobdadada/formula-zoo/)

