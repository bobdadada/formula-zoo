> 收集各学科中十分有用的公式，证明，算法等等

*约定*

- 除目录外，具体内容由中英文书写。
- 目录树按照**学科类别-具体项-项子类**划分。大部分的**具体项**都是按照blog的风格书写。当**具体项**不易在一个页面呈现时，其内容会按照合适的结构划分。