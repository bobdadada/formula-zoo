### Notations

| notation  | description                                 |
| --------- | ------------------------------------------- |
| $x,y,z$   | labels of the axes of Cartesian coordinate. |
| $w_0$     | waist radius of Gaussian beam               |
| $\lambda$ | wavelength                                  |
| $k$       | magnitude of wave vector                    |

![Gaussian Beam](pics/gaussian_beam.jpg)

### Hermite-Gaussian Beam

The $mn$th order normalized Hermite-Gaussian beam with waist at $z=0$ propagating in the $z$ direction is given by
$$
u_{mn}(x,y,z)=\frac{C_{mn}}{\sqrt{1+z^2/z_R^2}}\psi_{m}\left(\frac{\sqrt{2}x}{w}\right)\psi_{n}\left(\frac{\sqrt{2}y}{w}\right)\\
\cdot\exp\left[-\frac{jk}{2R}(x^2+y^2)\right]e^{j(m+n+1)\phi}
$$
where
$$
z_R=\frac{\pi w^2_0}{\lambda}
$$
$z_R$ is also called by Rayleigh length or Rayleigh range in optics and laser science,
$$
R=R(z)=z\left[1+\left(\frac{z_R}{z}\right)^2\right]
$$

$$
w=w(z)=w_0\left[1+\left(\frac{z}{z_R}\right)^2\right]^{1/2}
$$

$$
\tan \phi=\frac{z}{z_R}
$$

$$
C_{mn}=\left(\frac{2}{w^2_0 \pi 2^{m+n} m! n!}\right)^{1/2}
$$

And
$$
\psi_m(\xi)\equiv H_{m}(\xi)e^{-\xi^2/2}
$$
is the $m$th order Hermite-Gaussian mode to equation
$$
-\frac{d^2\psi_m}{d\xi^2}+\xi^2\psi_m=\lambda_m\psi_m
$$
with eigenvalue $\lambda_m$ given by
$$
\lambda_m=2\left(m+\frac{1}{2}\right)
$$
The normalization factor $C_m$ of $\psi_{m}(\xi)$ is given by
$$
C_m=\frac{1}{\pi^{1/4}\sqrt{2^m m!}}
$$
$m$ can be one of $0,1,2,\dots$.See <a class="refer">[2]</a> for more details. Note the only difference between each Gaussian beam $u_{mn}(x,y,z)$ of different order is the transverse field $\psi_{m}\left(\frac{\sqrt{2}x}{w}\right)\psi_{n}\left(\frac{\sqrt{2}y}{w}\right)$ and the additional phase $e^{j(m+n+1)\phi}$, the latter is also called Gouy phase shift<sup><a class="refer">[3]</a></sup>. Therefore, the propagation characteristics of each order of Gaussian beams are the same.

### Transformation of Hermite-Gaussian Beam

The characteristic of transformation can be fully determined by factor $q=z+jz_R$, and
$$
\frac{1}{q}=\frac{1}{z+jz_{R}}=\frac{z-jz_{R}}{z^2+z^2_{R}}=\frac{1}{R}-j\frac{\lambda}{\pi w^2}
$$
The last term of the equation shows that the $q$ parameter can be directly obtained with the physical parameters of the Gaussian beam, independent of the coordinate axis $z$.

#### Free Space

At a plane $z'$ at distance $d$ from the plane $z$
$$
q'=z'+jz_R=z+d+jz_R=q+d
$$

#### Thin Lens

If a Gaussian beam of the form is incident on a thin lens with focal length $f'$, then a phase shift is introduced at the plane of the lens. It gives
$$
-\frac{jk(x^2+y^2)}{2R}+\frac{jk(x^2+y^2)}{2f'}=-\frac{jk(x^2+y^2)}{2R'} \\
\Rightarrow \frac{1}{R'}=\frac{1}{R}-\frac{1}{f'}
$$
Using the definition of the $q$ parameter, one concludes that
$$
\frac{1}{q'}=\frac{1}{q}-\frac{1}{f'}\\
\text{or} \ q'=\frac{q}{-q/f'+1}
$$

**changes of the waist**

Set the plane of $z = 0$ to coincide with the thin lens. See figure below.

!> Please note that the definition of the symbol here is slightly different from the definition above.

![transformation by thin lens](pics/gb_thin_lens.svg)

With the characteristics of Gaussian beams and the notation in the figure, we get
$$
w = w_0\left[1+\left(\frac{\lambda s}{\pi{w_0}^2}\right)^2\right]^{1/2}, \ R= s\left[1+\left(\frac{\pi w_0^2}{\lambda s}\right)^2\right]
$$
and
$$
w'_0 = \frac{w'}{\left[1+\left(\frac{\pi{w'}^2}{\lambda R'}\right)^2\right]^{1/2}}, \ s' =  \frac{R'}{1+\left(\frac{\lambda R'}{\pi{w'}^2}\right)^2}
$$
With our notations, the features of thin lens imply that
$$
w' = w \ \text{and} \ \frac{1}{R'}-\frac{1}{R} = \frac{1}{f'}
$$
Then with the order following
$$
(w_0,s)\rightarrow(w,R)\rightarrow(w',R')\rightarrow(w'_0,s')
$$
one can get radius and position of the waist of the Gaussian beam after passing through the thin lens.

#### Mirror

A mirror of radius $R_0$ reflects the beam and changes the radius of curvature of the phase front. If we unfold the beam, we find that the incident phase delay $k(x^2+y^2)/(2R)$ is advanced by $2[k(x^2+y^2)/(2R_0)]$ because the path is shortened twice. Thus $R'$ of the unfolded reflected beam is given by
$$
\frac{1}{R'}=\frac{1}{R}-\frac{2}{R_0}
$$
A unfolded mirror acts like a lens with a focal distance $f'=R_0/2$.

![transformation by mirror](pics/gb_mirror.svg)

<div id="refer-anchor"></div>

### References

[1]: [Alignment of Gaussian beams](_assets/paper/alignment_of_gaussian_beams.pdf ':ignore :target=_blank')

[2]: [Waves and Fields in Optoelectronics; P: 108](_assets/book/waves-and-fields-in-optoelectronics.djvu ':ignore :target=_blank')

[3]: [Physical origin of the Gouy phase shift](_assets/paper/physical_origin_of_the_gouy_phase_shift.pdf ':ignore :target=_blank')

