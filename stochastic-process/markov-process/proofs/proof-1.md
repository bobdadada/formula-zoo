### [PROBLEM](../proofs#proof-1  ':class=path-append')

A transition matrix $\mathbf{P}$ is said to be doubly stochastic if the sum over each column equals one, that is $\sum_i{P_{ij}}=1\, \forall{i}$. If such a chain is irreducible and aperiodic and consists of $M$ states $1,2,\dots,M$ show that the limiting probabilities are given by
$$
\pi_j=\frac{1}{M}, \quad j=1,2,\dots,M
$$
**PROOF**:

A vector $\pi$ is called stationary distribution vector of a Markov process with transition matrix $\mathbf{P}$ if the elements of $\pi$ satisfies
$$
\pi=\pi\mathbf{P}, \quad \sum^{M}_{i=1}\pi_i=1, \, \forall \pi_i>0, \quad \sum^{M}_{j=1}{P_{ij}}=1
$$
which implies
$$
\pi_j=\sum^{M}_{i=1}\pi_i P_{ij} \tag{1}
$$
Since the process is a doubly stochastic process,
$$
\sum^{M}_{i=1}{P_{ij}}=1 \tag{2}
$$
Then from $(1)$ and $(2)$, we can get
$$
\begin{aligned}
1-\pi_j=\sum^{M}_{i=1}(1-\pi_i)P_{ij} \\
\Rightarrow 1_{M}-\pi = (1_{M}-\pi)\mathbf{P}
\end{aligned}
$$
where $1_M=(1,1,1,\dots,1)$ and $\sum^{M}_{i=1}(1-\pi_i)=M-1$. Thus $\frac{1_M-\pi}{M-1}$ is also a stationary solution. Since such a chain is irreducible and aperiodic, the stationary distribution is unique. We get a unique stationary distribution
$$
\begin{aligned}
\frac{1-\pi_i}{M-1}=\pi_i \\
\Rightarrow \pi_i = \frac{1}{M}
\end{aligned}
$$

<div align="center" style="font-size: 15px;">
    <a href="#/../proofs?id=proof-1" class='path-append'>Back to Contents</a> 
</div>


