- [证明 1](proofs/proof-1.md ':id=proof-1 :class=path-append')

A transition matrix $\mathbf{P}$ is said to be doubly stochastic if the sum over each column equals one, that is $\sum_i{P_{ij}}=1\, \forall{i}$. If such a chain is irreducible and aperiodic and consists of $M$ states $1,2,\dots,M$ show that the limiting probabilities are given by
$$
\pi_j=\frac{1}{M}, \quad j=1,2,\dots,M
$$
