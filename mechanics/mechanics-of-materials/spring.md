### Spring Constant

Spring constant is given by
$$
k = \frac{Gd^4}{8D^3 N}
$$

where $G$ is shear modulus of spring material(弹簧材料的切变模量), $d$ is wire diameter of spring (弹簧的线径), $D$ is diameter of spring(弹簧的直径), $N$ is effective number of turns of spring(弹簧的有效圈数). See figure below.

![spring](pics/spring.svg)

<div id="example-1" class="example">

<em>Example 1:</em>

A small spring made of alloy steel with $d=0.3(mm)$, $D=1.5(mm)$, $N=10$ and $G=80,000(N/mm^2)$. The spring constant of this spring is
$$
k=\frac{Gd^4}{8D^3 N}=\frac{80000\times 0.3^4}{8\times 1.5^3 \times10}=2.4(N/mm)
$$

</div>



